package main

import (
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var schema = `
CREATE TABLE IF NOT EXISTS "author" (
    "id" SERIAL PRIMARY KEY,
    "firstname" varchar(255) NOT NULL,
    "lastname" varchar(255) NOT NULL,
    "created_at" TIMESTAMP DEFAULT(Now()),
    "updated_at"  TIMESTAMP DEFAULT(Now())
);

CREATE TABLE IF NOT EXISTS "article" (
    "id" SERIAL PRIMARY KEY,
    "title" VARCHAR(255) NOT NULL UNIQUE,
    "body" TEXT,
    "author_id" INT,
    "created_at" TIMESTAMP DEFAULT(Now()),
    "updated_at"  TIMESTAMP DEFAULT(Now()),
    CONSTRAINT fk_author FOREIGN KEY(author_id) REFERENCES author(id)
);

INSERT INTO author (firstname, lastname) VALUES ('Jason', 'Moiron') ON CONFLICT DO NOTHING;
INSERT INTO author (firstname, lastname) VALUES ('John', 'Doe') ON CONFLICT DO NOTHING;

INSERT INTO article (title, body, author_id) VALUES ('Lorem1', 'Lorem ipsum1', 1) ON CONFLICT DO NOTHING;
INSERT INTO article (title, body, author_id) VALUES ('Lorem2', 'Lorem ipsum2', 2) ON CONFLICT DO NOTHING;
`

type Content struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}

type Article struct {
	ID        int        `json:"id"`
	Content              // Promoted fields
	Author    Person     `json:"author"` // Nested structs
	CreatedAt *time.Time `json:"-"`
}

type Person struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

func main() {
	psqlConnString := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		"localhost",
		5432,
		"postgres",
		"123",
		"postgres",
	)

	db, err := sqlx.Connect("postgres", psqlConnString)
	if err != nil {
		log.Panic(err)
	}

	db.MustExec(schema)

	rows, err := db.Query(
		"SELECT ar.id, ar.title, ar.body, ar.created_at, au.firstname, au.lastname FROM article AS ar JOIN author AS au ON ar.author_id = au.id WHERE ar.id = $1",
		1,
	)
	if err != nil {
		log.Panic(err)
	}
	defer rows.Close()

	var arr []Article
	for rows.Next() {
		var a Article
		err = rows.Scan(&a.ID, &a.Title, &a.Body, &a.CreatedAt, &a.Author.Firstname, &a.Author.Lastname)
		arr = append(arr, a)
		if err != nil {
			log.Panic(err)
		}
	}

	fmt.Println(arr)

	res, err := db.NamedExec(
		`INSERT INTO article (title, body, author_id) VALUES (:t, :b, :a_id)`,
		map[string]interface{}{
			"t":    "Bin",
			"b":    "Smuth",
			"a_id": 2,
		},
	)

	if err != nil {
		log.Println("----->", err)
	}

	fmt.Printf("%#v", res)

	res2, err2 := db.Exec(
		`UPDATE article SET title=$1, body=$2, updated_at=now() WHERE id = $3`,
		"1111",
		"22222222",
		3,
	)

	if err2 != nil {
		log.Panic(err2)
	}

	num, err2 := res2.RowsAffected()
	if err2 != nil {
		log.Panic(err2)
	}
	fmt.Println(num)

	// Get by id
	rows1, err := db.Query(
		"SELECT firstname, lastname from article where id= $1",
		3,
	)
	if err != nil {
		log.Panic(err)
	}
	defer rows1.Close()
	var arr1 []Person
	for rows1.Next() {
		var a Person
		err = rows.Scan(&a.Firstname, &a.Lastname)
		arr1 = append(arr1, a)
		if err != nil {
			log.Panic(err)
		}
	}
	fmt.Println("---->", arr1)
	// Get all
	rows2, err := db.Query(
		"SELECT firstname, lastname from article ")
	if err != nil {
		log.Panic(err)
	}
	defer rows2.Close()
	var arr2 []Person
	for rows1.Next() {
		var a Person
		err = rows.Scan(&a.Firstname, &a.Lastname)
		arr2 = append(arr2, a)
		if err != nil {
			log.Panic(err)
		}
	}
	fmt.Println("---->", arr2)
	// Delete
	res3, err := db.Exec(
		`DELETE FROM author WHERE id = $3`,
		3,
	)

	if err != nil {
		log.Panic(err)
	}

	nums, err := res3.RowsAffected()
	if err != nil {
		log.Panic(err)
	}
	fmt.Println(nums)

}
